# R-Code for corpus analysis procedures

## Overview

The R-Code provided in this repository is tailored to the legal case study collection *Der neue Pitaval. Eine Sammlung der interessantesten Criminalgeschichten aller Laender aus aelterer und neuerer Zeit.* (1842-1890), edited by J. E. Hitzig and W. Haering (W. Alexis). It was developed within the scope of my masterthesis *Between law and literature: Literary corpus analysis of the case study collection Der Neue Pitaval (1842-1854)* at TU Darmstadt, department of Digitale Literaturwissenschaft.

The corpus analysis toolkit is designed to execute the following steps:
* **word frequency**
* **lexical measures** as type-token ratio, mean word usage, hapax richness
* **sentence length**
* **collocations**
* **keyword in context** (KWIC)
* **POS based dispersion plots**
* **POS tagging**
* **topic modelling** (TM)
* unsupervised clustering (rudimentary)

There are also files with auxiliary code, e.g. corpusFunctions.R and freqFunctions.R comprising functions which are called by some code in some other files. So ensure to save these files to your working directory as well.

Despite code is extensively commented you need an understandig of the R language in order to adapt code snippets if needed. Also make sure to set the right file path according to your local directory.

## Input data

The code will perfom some trimming of the title as well as metadata extraction. Therefore it is important that input files are named as in this example:

	Bd7_7_1845_JochimHinrichRamcke_1837-1843.txt

## Contact

Please inform me if you have got any trouble with this code. Feel free to contact me for suggestions for improvement.

<constanze-hahn@web.de>

## Citation

	@misc{pitavalAnalysis,
	author = "Constanze Hahn",
	title = "R-Code for corpus analysis procedures",
	howpublished = "\url{https://gitlab.com/cohaDigiLit/PitavalCorpusAnalysis}",
	year = 2017,
	note = {Developed within the scope of my masterthesis Between law and literature: Literary corpus analysis of the case study collection Der Neue Pitaval (1842--1854) at TU Darmstadt, department of Digitale Literaturwissenschaft}
	}