###----- corpus frequencies, lexical measures -----###
setwd("C:/Users/CH/Projekte/TextAnalysisWithR")
source("code/corpusFunctions.R")
library(stylo)
library(gtools)
library(xlsx)
library(openxlsx)
library(ggplot2)

# R object with the path to the text files directory
input.dir <- "data/plainText"

# output directory
output.dir <- "results/"

# retrieve all file names
corpus.files.v <- dir(path = input.dir, "\\.txt$")

# sort character arrays containing letters and numbers
# and sort numbers by 1,2,3...10,11,.. instead of 1,10,11,....
corpus.files.v <- mixedsort(corpus.files.v)

stopWords.v <- scan(file.path("data/stoplist/pitavalStoplist.txt"),
                    what = "character", sep = "\n" , encoding = "UTF-8", quiet = T)

# a list that contains name and word vectors for every file
corpus.l <- makeWordVectorList(corpus.files.v, input.dir)

# list objects to hold the results
corpus.raws.l <- list()
corpus.freqs.l <- list()

# iterate through every story of corpus
for (i in 1:length(corpus.files.v)){
  text.v <- scan(file.path(input.dir, corpus.files.v[i]),
                 what = "character", sep = "\n" , encoding = "UTF-8", quiet = T)
  title <- gsub("\\..*","", corpus.files.v[i])
  title <- gsub("^[[:alnum:]]*_[[:digit:]]*_|$", "", title) 
  # tokenization
  tokens.v <- tokenizeMyText(text.v)
  # list of raw counts of every word type for each text
  corpus.raws.l[[title]] <- getWordAbsFreqTable(tokens.v)
  # list of relative frequencies of every word type for each text
  corpus.freqs.l[[title]] <- getWordRelFreqTable(tokens.v)
}
write.xlsx(corpus.raws.l, file = paste0(output.dir, "frequencies/corpus_absfreqs.xlsx"),
           row.names = T , col.names = T, showNA = F)

## ----- Frequencies ----- ##
## following code produces frequency tables in different formats and layout ##

# outputs 50 mfw for every text
# saved as txt-file
# texts are arranged below each other
for (i in 1:length(corpus.freqs.l)) {
  # table with sorted mfw
  sorted.text.freqs.t <- sort(corpus.freqs.l[[i]], decreasing = TRUE)
  out.file <- paste0(output.dir, "frequencies/freqlist50mfw.txt")
  cat(if (i > 1) "\n", names(corpus.freqs.l)[i], "\n", file = out.file, sep = "", append = i > 1)
  write.table(sorted.text.freqs.t[1:50], out.file,
              sep = ";", dec = ",", #fileEncoding = "UTF-8"
              row.names = F,
              col.names = F,
              append = TRUE,
              quote = F)
}

# outputs 10 mfw for every text
# saved as csv-file
# head specifies number of mfw
for (i in 1:length(corpus.l)) {
  freqlist.t <- make.frequency.list(corpus.l[i], value = TRUE, head = 10)
  write.table(freqlist.t, paste0(output.dir, "frequencies/freqlist10mfw.csv"),
              sep = ";", dec = "," , row.names = T , col.names = NA,  append = TRUE)
}

# outputs 100 mfw for every text
# saved as xslx-file
# texts are arranged side by side
sorted.text.freqs.m <- NULL
for (i in 1:length(corpus.freqs.l)) {
  sorted.text.freqs.t <- sort(corpus.freqs.l[[i]], decreasing = TRUE)
  limit.text.freqs <- sorted.text.freqs.t[1:100]
  words.col <- rbind(paste(names(corpus.freqs.l)[i]), cbind(names(limit.text.freqs)))
  freq.col <- rbind("", cbind(limit.text.freqs))
  words.freqs <- cbind(words.col, freq.col)
  sorted.text.freqs.m <- cbind(sorted.text.freqs.m, words.freqs)
}
sorted.text.freqs.df <- as.data.frame(sorted.text.freqs.m, stringsAsFactors = F)
# change every 2nd column into numeric
for (i in 1:ncol(sorted.text.freqs.df)) {
  if (i %% 2){
    next
  }
  # if as.character is omitted factors will be converted to their numeric storage values,
  # not necessary when stringsAsFactors = F
  sorted.text.freqs.df[, i] <- as.numeric(sorted.text.freqs.df[, i])
}
write.xlsx(sorted.text.freqs.df,
           paste0(output.dir, "frequencies/freqlist100mfw.xlsx"),
           col.names = F, row.names = F, showNA = F)

# --- frequency plots --- #
# graph with relative frequencies of top 50 words
# for six selected texts, specified in for-loop
par(pch=22)
par(mfrow=c(2,3)) # all plots on one page
for(i in 31:36) {
  sorted.text.freqs.t <- sort(corpus.freqs.l[[i]], decreasing = TRUE)
  heading = names(corpus.freqs.l[i])
  plot(sorted.text.freqs.t[1:50],
       type = "b",
       main = heading,
       xlab = "",
       ylab = "Percentage",
       xaxt = "n")
  axis(1, 1:50, labels = names(sorted.text.freqs.t[1:50]), las = 2)
}

# stopwords removed
# graph with relative frequencies of top 50 words
# for six selected texts, specified in for-loop
corpus.rmstop.l <- removeWords(corpus.l, stopWords.v)
corpus.rmstop.freqs.l <- list()
for (i in 1:length(corpus.rmstop.l)){
  tokens.v <- corpus.rmstop.l[[i]]
  title <- gsub("^[[:alnum:]]*_[[:digit:]]*_|$", "", names(corpus.rmstop.l)[i])
  # list of relative frequencies of every word type for each text
  corpus.rmstop.freqs.l[[title]] <- getWordRelFreqTable(tokens.v)
}

par(pch=22)
par(mfrow=c(2,3)) # all six plots on one page
par(mar = c(8, 4, 2, 0.5) + 0.2) # adjust margins to make axis labels fit on page
for(i in 31:36) {
  sorted.text.freqs.t <- sort(corpus.rmstop.freqs.l[[i]], decreasing = TRUE)
  heading = names(corpus.rmstop.freqs.l[i])
  plot(sorted.text.freqs.t[1:50],
       type = "b",
       main = heading,
       xlab = "",
       ylab = "Percentage",
       xaxt = "n")
  axis(1, 1:50, labels = names(sorted.text.freqs.t[1:50]), las = 2)
}

## --- frequencies for the entire corpus --- ##
# plot and save 100 mfw of entire corpus
alltokens.v <- c()
for (i in 1:length(corpus.files.v)){
  text.v <- scan(file.path(input.dir, corpus.files.v[i]),
                 what = "character", sep = "\n",
                 encoding = "UTF-8", quiet = T)
  tokens.v <- tokenizeMyText(text.v)
  alltokens.v <- c(alltokens.v, tokens.v)
}
freqs.rel.t <- getWordRelFreqTable(alltokens.v)
complete.df <- as.data.frame(freqs.rel.t, stringsAsFactors = F)
colnames(complete.df) <- c ("Unique_Word_Type", "Freq")
complete.df <- complete.df[order(complete.df$Freq, decreasing = TRUE), ]
complete.df$Unique_Word_Type <- with(complete.df,
                                     reorder(Unique_Word_Type, -Freq))
pdf(paste0(output.dir, "frequencies/100mfwplot.pdf"), width = 12, height = 6)
ggplot(head(complete.df, 100)) + geom_point(aes(x=Unique_Word_Type, y=Freq)) +
  theme(axis.text.x=element_text(angle=90, hjust=1))
dev.off()

# plot and save 100 mfw of entire corpus
# without stopwords
alltokens.v <- c()
for (i in 1:length(corpus.files.v)){
  text.v <- scan(file.path(input.dir, corpus.files.v[i]),
                 what = "character", sep = "\n" , encoding = "UTF-8", quiet = T)
  tokens.v <- tokenizeMyText(text.v)
  tokens.v <- tokens.v[!tokens.v %in% stopWords.v]
  alltokens.v <- c(alltokens.v, tokens.v)
}
freqs.rel.t <- getWordRelFreqTable(alltokens.v)
complete.df <- as.data.frame(freqs.rel.t, stringsAsFactors = F)
colnames(complete.df) <- c ("Unique_Word_Type", "Freq")
complete.df <- complete.df[order(complete.df$Freq, decreasing = TRUE), ]
complete.df$Unique_Word_Type <- with(complete.df, reorder(Unique_Word_Type, -Freq))
pdf(paste0(output.dir, "frequencies/100mfwplot_nostopwords.pdf"), width = 12, height = 6)
ggplot(head(complete.df, 100)) + geom_point(aes(x=Unique_Word_Type, y=Freq)) +
  theme(axis.text.x=element_text(angle=90, hjust=1))
dev.off()

## ----- Lexical Measures ----- ##
## the following creates a data matrix with columns for publication year, ##
## types, tokens, ttratio, mean word usage, hapaxes ##

## --- types, tokens --- ##
tokens.m <- do.call(rbind, lapply(corpus.raws.l, sum))
types.m <- do.call(rbind, lapply(corpus.raws.l, length))

## --- measures of lexical variety --- ##
# calculate type/token ratio
ttr.l <- lapply(corpus.raws.l, function(x) {length(x) / sum(x) * 100}) # length(corpus.raws.l[[1]])/sum(corpus.raws.l[[1]]) * 100
ttr.m <- do.call (rbind, ttr.l)
#ttr.m[order(ttr.m, decreasing = TRUE), ]
#par(mfrow=c(1,1))
#plot(ttr.m, type = "h")

#calculate mean word usage/mean word frequency
# mean word usage with absolute freqs
mean.word.use.m <- do.call(rbind, lapply(corpus.raws.l, mean))
# mean word usage with relative freqs
mean.word.use.freqs.m <- do.call(rbind, lapply(corpus.freqs.l, mean))
#mean.word.use.m[order(mean.word.use.m, decreasing = TRUE), ]

## --- hapax richness --- ##
# calculate a sum of all types that appear only once
corpus.hapax.v <- sapply(corpus.raws.l, function(x) {sum(x == 1)}) # values are all one
# the ratio of hapaxes for each story
hapax.ratio.m <- (corpus.hapax.v / tokens.m) # if percentage: * 100
#barplot(hapax.ratio.m, beside = T, col = "grey", names.arg = seq(1:length(corpus.raws.l)))
lex.measures.m <- cbind(tokens.m, types.m, ttr.m, mean.word.use.m, mean.word.use.freqs.m, hapax.ratio.m)
colnames(lex.measures.m) <- c("tokens","types", "type_token_ratio", "raw_mean_word_usage", "rel_mean_word_usage", "hapaxes")

## add column with year of publication ##
pbyr.v <- c()
for (i in 1:length(corpus.freqs.l)) {
  yrs <- gsub("_[[:alnum:]]*_?[[:alnum:]]*_?[[:digit:]*-?[:digit:]]*$", "", names(corpus.freqs.l)[i])
  pbyr.v <- c(pbyr.v, yrs)}
lex.measures.df <- cbind(Publ_Year = pbyr.v, data.frame(lex.measures.m))

# delete publication year from titles to make them shorter
for (i in 1:length(row.names(lex.measures.df))) {
  row.names(lex.measures.df)[i] <- gsub("^[[:digit:]]*_", "", row.names(lex.measures.df)[i])
}

#--- save data matrix ---#
# save complete data matrix of publication year, types, tokens, ttr, mean word usage(raw and freq), hapaxes
write.xlsx(lex.measures.df, file = paste0(output.dir, "measures/lexmeasures.xlsx"), sheetName = "Lexical Measures",
           row.names = T , col.names = T, showNA = F)

##----- load excel-file -----##
## Note that the column "title_groups" was added manually in excel ##
lex.measures.df <- read.xlsx("results/measures/lexmeasures.xlsx", 1, rowNames = TRUE)

#--- plot corpus ---#
# plot and save corpus type token ratio for each text
cols <- c("black", "red", "green3", "blue", "orange")
cols_groups <- cols[lex.measures.df$title_groups]
forms <- c(21,22,24,25,23)
pch_groups <- forms[lex.measures.df$title_groups]

pdf("results/measures/plot_corpus_ttr_groups.pdf", width = 14, height = 7)
plot(c(1:length(corpus.files.v)), lex.measures.df$type_token_ratio,
     main = "Corpus Type Token Ratio",
     xlab = "corpus texts",
     ylab = "ttr",
     xaxs="i", yaxs="i",
     xlim=c(0, (length(corpus.files.v)+1)),
     ylim = c((min(unlist(lex.measures.df$type_token_ratio), na.rm = TRUE)-0.5),
              (max(unlist(lex.measures.df$type_token_ratio), na.rm = TRUE)+0.5)),
     lty = 2,
     xaxt = "n",
     grid(),
     bg = cols_groups,
     col = "black",
     pch = pch_groups)
lines(c(1:length(corpus.files.v)), lex.measures.df$type_token_ratio, type = "h")
axis(1, at = seq(1,length(corpus.files.v), by = 4), las = 2)
abline(h = 31, col="blue")
abline(v = 1, col = "black", lty = 5)
text(4, 49, "1842", col = "black")
abline(v = 20, col = "black", lty = 5)
text(23, 48, "1843", col = "black")
abline(v = 40, col = "black", lty = 5)
text(43, 49, "1844", col = "black")
abline(v = 59, col = "black", lty = 5)
text(62, 49, "1845", col = "black")
abline(v = 72, col = "black", lty = 5)
text(75, 49, "1846", col = "black")
abline(v = 89, col = "black", lty = 5)
text(92, 49, "1847", col = "black")
abline(v = 108, col = "black", lty = 5)
text(111, 49, "1848", col = "black")
abline(v = 115, col = "black", lty = 5)
text(118, 49, "1849", col = "black")
abline(v = 121, col = "black", lty = 5)
text(124, 49, "1850", col = "black")
abline(v = 145, col = "black", lty = 5)
text(148, 49, "1851", col = "black")
abline(v = 156, col = "black", lty = 5)
text(159, 55, "1852", col = "black")
abline(v = 187, col = "black", lty = 5)
text(190, 49, "1853", col = "black")
abline(v = 196, col = "black", lty = 5)
text(199, 49, "1854", col = "black")
legend(
  x = 10, y= 60.25,
  ## note that German umlauts and special characters may cause encoding troubles
  legend = c("Taeter","Opfer","Tat","Raeuber", "literarisierter Titel"),
  col = "black",
  pch = c(21,22,24,25,23), bg="white",
  pt.bg = cols)
dev.off()

# plot and save corpus mean word usage for each text
pdf("results/measures/plot_corpus_mwu_groups.pdf", width = 14, height = 7)
plot(c(1:length(corpus.files.v)), lex.measures.df$raw_mean_word_usage,
     main = "Corpus Mean Word Usage",
     xlab = "corpus texts",
     ylab = "mean word use",
     xaxs="i", yaxs="i",
     xlim=c(0, (length(corpus.files.v)+1)),
     ylim = c((min(unlist(lex.measures.df$raw_mean_word_usage), na.rm = TRUE)-0.5),
              (max(unlist(lex.measures.df$raw_mean_word_usage), na.rm = TRUE)+0.5)),
     lty = 2,
     xaxt = "n",
     grid(),
     bg = cols_groups,
     col = "black",
     pch = pch_groups)
lines(c(1:length(corpus.files.v)), lex.measures.df$raw_mean_word_usage, type = "h")
axis(1, at = seq(1,length(corpus.files.v), by = 4), las = 2)
abline(h = 3.484, col="blue")
abline(v = 1, col = "black", lty = 5, lwd = 1.5)
text(4.5, 5.7, "1842", col = "black")
text(4, 5.4, "Bd.1", col = "black", cex = 0.75)
abline(v = 11, col = "black", lty = 2)
text(13.5, 5.4, "Bd.2", col = "black", cex = 0.75)
abline(v = 20, col = "black", lty = 5, lwd = 1.5)
text(23.5, 5.7, "1843", col = "black")
text(23, 5.4, "Bd.3", col = "black", cex = 0.75)
abline(v = 28, col = "black", lty = 2)
text(30.5, 5.4, "Bd.4", col = "black", cex = 0.75)
abline(v = 40, col = "black", lty = 5, lwd = 1.5)
text(43.5, 7, "1844", col = "black")
text(43, 6.7, "Bd.5", col = "black", cex = 0.75)
abline(v = 49, col = "black", lty = 2)
text(51.5, 6.7, "Bd.6", col = "black", cex = 0.75)
abline(v = 59, col = "black", lty = 5, lwd = 1.5)
text(62.5, 7, "1845", col = "black")
text(62, 6.7, "Bd.7", col = "black", cex = 0.75)
abline(v = 66, col = "black", lty = 2)
text(68.5, 6.7, "Bd.8", col = "black", cex = 0.75)
abline(v = 72, col = "black", lty = 5, lwd = 1.5)
text(75.5, 7, "1846", col = "black")
text(75, 6.7, "Bd.9", col = "black", cex = 0.75)
abline(v = 81, col = "black", lty = 2)
text(84, 6.7, "Bd.10", col = "black", cex = 0.75)
abline(v = 89, col = "black", lty = 5, lwd = 1.5)
text(92.5, 7, "1847", col = "black")
text(92.5, 6.7, "Bd.11", col = "black", cex = 0.75)
abline(v = 101, col = "black", lty = 2)
text(104, 6.7, "Bd.12", col = "black", cex = 0.75)
abline(v = 108, col = "black", lty = 5, lwd = 1.5)
text(111, 7, "1848", col = "black")
text(111, 6.7, "Bd.13", col = "black", cex = 0.75)
abline(v = 115, col = "black", lty = 5, lwd = 1.5)
text(118, 7, "1849", col = "black")
text(118, 6.7, "Bd.14", col = "black", cex = 0.75)
abline(v = 121, col = "black", lty = 5, lwd = 1.5)
text(124.5, 7, "1850", col = "black")
text(124.5, 6.7, "Bd.15", col = "black", cex = 0.75)
abline(v = 134, col = "black", lty = 2)
text(137, 6.7, "Bd.16", col = "black", cex = 0.75)
abline(v = 145, col = "black", lty = 5, lwd = 1.5)
text(148.5, 7, "1851", col = "black")
text(148.5, 6.7, "Bd.17", col = "black", cex = 0.75)
abline(v = 156, col = "black", lty = 5, lwd = 1.5)
text(159.5, 7, "1852", col = "black")
text(159.5, 6.7, "Bd.18", col = "black", cex = 0.75)
abline(v = 180, col = "black", lty = 2)
text(183, 6.7, "Bd.19", col = "black", cex = 0.75)
abline(v = 187, col = "black", lty = 5, lwd = 1.5)
text(191, 7, "1853", col = "black")
text(191, 6.7, "Bd.20", col = "black", cex = 0.75)
abline(v = 196, col = "black", lty = 5, lwd = 1.5)
text(199.5, 7, "1854", col = "black")
text(199.5, 6.7, "Bd.21", col = "black", cex = 0.75)
legend(
  x ="topleft",
  ## note that German umlauts and special characters may cause encoding troubles
  legend = c("Taeter","Opfer","Tat","Raeuber", "literarisierter Titel"),
  col = "black",
  pch = c(21,22,24,25,23), bg="white",
  pt.bg = cols)
dev.off()

# plot and save corpus relative mean word usage for each text
pdf("results/measures/plot_corpus_rel_mwu_groups.pdf", width = 14, height = 7)
plot(c(1:length(corpus.files.v)), lex.measures.df$rel_mean_word_usage,
     main = "Corpus Relative Word Usage Mean",
     xlab = "corpus texts",
     ylab = "relative mean word use",
     xaxs="i", yaxs="i",
     xlim=c(0, (length(corpus.files.v)+1)),
     ylim = c((min(unlist(lex.measures.df$rel_mean_word_usage), na.rm = TRUE)-0.01),
              (max(unlist(lex.measures.df$rel_mean_word_usage), na.rm = TRUE)+0.01)),
     lty = 2,
     xaxt = "n",
     grid(),
     bg = cols_groups,
     col = "black",
     pch = pch_groups)
lines(c(1:length(corpus.files.v)), lex.measures.df$rel_mean_word_usage, type = "h")
axis(1, at = seq(1,length(corpus.files.v), by = 4), las = 2)
abline(h = 0.057, col="blue")
abline(v = 1, col = "black", lty = 5)
text(4, 0.28, "1842", col = "black")
abline(v = 20, col = "black", lty = 5)
text(23, 0.28, "1843", col = "black")
abline(v = 40, col = "black", lty = 5)
text(43, 0.28, "1844", col = "black")
abline(v = 59, col = "black", lty = 5)
text(62, 0.28, "1845", col = "black")
abline(v = 72, col = "black", lty = 5)
text(75, 0.28, "1846", col = "black")
abline(v = 89, col = "black", lty = 5)
text(92, 0.28, "1847", col = "black")
abline(v = 108, col = "black", lty = 5)
text(111, 0.28, "1848", col = "black")
abline(v = 115, col = "black", lty = 5)
text(118, 0.28, "1849", col = "black")
abline(v = 121, col = "black", lty = 5)
text(124, 0.28, "1850", col = "black")
abline(v = 145, col = "black", lty = 5)
text(148, 0.28, "1851", col = "black")
abline(v = 156, col = "black", lty = 5)
text(159, 0.28, "1852", col = "black")
abline(v = 187, col = "black", lty = 5)
text(190, 0.28, "1853", col = "black")
abline(v = 196, col = "black", lty = 5)
text(199, 0.28, "1854", col = "black")
legend(
  x = 6, y = 0.38,
  ## note that German umlauts and special characters may cause encoding troubles
  legend = c("Taeter","Opfer","Tat","Raeuber", "literarisierter Titel"),
  col = "black",
  pch = c(21,22,24,25,23), bg="white",
  pt.bg = cols)
dev.off()


cor(lex.measures.m[,1], lex.measures.m[,5])
# strong negative correlation between story length and number of hapax

ranks <- order(hapax.ratio.m, decreasing = TRUE)

barplot(lex.measures.m[,1],
        beside = TRUE,
        main = "Correlation of tokens and hapaxes",
        col = c("darkblue", "red"),
        names.arg = seq(1:length(corpus.raws.l)),
        ylab = "Number of words")


#### not important ####
# Save my workspace to complete_image.RData in the
# data folder of my working directory
save.image(file = "projectimage.RData")
